# Execrcises

## Week 1

### Simple Output

#### Hello, World!

Input: none

Output:

```
Hello, World!
```

#### Toronto

Input: none

Output:

```
#@)₴?$0
Toronto TV
```

#### Mc'Donalds Order Number 1

Input: none

Output:

```
# # ### ###
# # #     #
### ###   #
  #   #   #
  # ###   #
```

#### Bart's Blackboard Detention Quote 1

Input: none

Output:

```
I WILL NOT SELL SCHOOL PROPERTY
I WILL NOT SELL SCHOOL PROPERTY
I WILL NOT SELL SCHOOL PROPERTY
I WILL NOT SELL SCHOOL PROPERTY
I WILL NOT SELL SCHOOL PROPERTY
I WILL NOT SELL SCHOOL PROPERTY
I WILL NOT SELL SCHOOL PROPERTY
I WILL NOT SELL SCHOOL PROPERTY
I WILL NOT SELL SCHOOL PROPERTY
```

#### Mc'Donalds Order Number 2

Input: none

Output:

```
 #######     ##    #######
##     ##  ####   ##     ##
##     ##    ##   ##     ##
 #######     ##    ########
##     ##    ##          ##
##     ##    ##   ##     ##
 #######   ######  #######
```

#### Quote from office

Input: none

Output:

```
"The worst thing about prison was the dementors." – Michael "Prison Mike" Scott, Season 3, "The Convict"
```

#### Raven

Input: none

Output:

```
Once upon a midnight dreary, while I pondered, weak and weary,
Over many a quaint and curious volume of forgotten lore—
While I nodded, nearly napping, suddenly there came a tapping,
As of some one gently rapping, rapping at my chamber door—
"'Tis some visitor," I muttered, "tapping at my chamber door—
               Only this and nothing more."
```

#### Leet

Input: none

Output:

```
Hello: 73g3|\||)_\\/\\/()R|)\.
Your new password is: txf?VwnQU-D^+^^j#t9uY2eu!*JZz3Ck

```

#### Roulette 1

Description: print output of five roulette games, each number should be random

Example:

```
./roulette
Roulette number is 5
Roulette number is 31
Roulette number is 5
Roulette number is 12
Roulette number is 6
```

### Simple Input

#### Square Area

Description: your program shoud be able to calculate area of a square
based on a given side

Input: (integer) square side

Example:

```sh
./square_area
Square side is 5
Therefore
Square area is 25
```

#### Bouquet Price

Description: your program shoud be able to calculate price of a bouquet
based on a given price per rose and quantity

Input: (integer) price of a rose, (integer) - quantity of roses

Example:

```sh
./bouquet_price
Price per rose 65
Quantity 5
Bouquet price is 325
```

#### Buckwheat Price

Description: your program shoud be able to calculate buckwheat purchase price
based on a buckwheat price of 49.5 uah per kg and an input of how much money
user is going to pay in uah

Input: (integer) money

Example:

```sh
./buckwheat_price
The price of buckwheat is 49.50 uah
Your money 98
You will get 2.00 kg of buckwheat
```

#### Greeting

Description: greet a user base on their name

Input - (string) name of a person

Example

```sh
./greeting
What is your name? Mike
Hello, Mike!
```

### Conditionals

#### Filter coffee proportion

Description: user shpuld enter amount of coffee in grams and
an amount of water in ml and the program should prompt the strenght of a coffee
based on a proportion of 15 mg of coffee to 250 ml of water

Input: (integer) coffer in mg, (integer) water in ml

Example:

```
./coffee_proportion
Enter the coffee proportions:
Blend coffee amount 30
Clean water amount 500
The coffee is just right
```

```
./coffee_proportion
Enter the coffee proportions:
Blend coffee amount 20
Clean water amount 500
The coffee is too watery
```

```
./coffee_proportion
Enter the coffee proportions:
Blend coffee amount 30
Clean water amount 400
The coffee is too strong
```

#### Seasons 1

Description: ask a user for a month number between 1 and 12 inclusive
and print a corresponding month

Input: (integer) month between 1 and 12 inclusive

```
./seasons
Month index is 3
It is March
```

#### Car speed

Description: enter a speed of a car if it's higher that allowed limit of 50 km/h
prompt a user that speed it too high, if it's lower say that is it ok,
when speed os 0 say that it is pefect, and if it is negative the car is
probably broken, also when the speed reacher 420, you should say nice

Input: (integer) speed of a car

Example:

```
./car_speed
Car speed: 60
The speed is to high
Please slow down
```

```
./car_speed
Car speed: 40
The speed is ok
```

```
./car_speed
Car speed: 0
The speed is perfect
```

```
./car_speed
Car speed: 420
Nice!
But still to high
```

```
./car_speed
Car speed: -20
Your car is broken
Please visit a repair shop
```

#### Roulette 2

Description: ask a user of a number to bet on and spin the roulette wheel,
notify a user if they won, lose, or entered a wron number

Input: (integer) user's bet between 0 to 36 inclusive

Example:

```
./roulette
You bet on 12
Roulette number is 9
You lose
```

```
./roulette
You bet on 9
Roulette number is 9
You won
```

```
./roulette
You bet on 99
Error: unacceptable bet
Please, choose a number between 0 and 36 inclusive next time
```

#### Seasons 2

Description: ask a user for a month number between 1 and 12 inclusive
and print a corresponding month and season

Input: (integer) month between 1 and 12 inclusive

Example:

```
./seasons
Month index is 3
It is March of Spring
```

### Loops

#### Bart's Blackboard Detention Quote 2

Descrition: principal enters a positive number of how many times Bart should
write detention sentace on a black board, then print it this many times in caps

Input: (integer) number of rows

Output:

```
./bart_detention
Number of rows: 3
I WILL NOT SELL SCHOOL PROPERTY
I WILL NOT SELL SCHOOL PROPERTY
I WILL NOT SELL SCHOOL PROPERTY
```

#### Countdown 1

Descrition: user enters positive number, then the count down starts from
number to 1 and them BOOM!

Input: (integer) countdown

Example:

```
./countdown
Countdown: 5
Beep 5
Boop 4
Beep 3
Boop 2
Beep 1
BOOM!
```

#### Sum

Descrition: Create a list of random numbers that should have length from 0 to 10

Output the list to the console

Calculate the sum of all numbers in the list

Example:

```
./sum
1. 5
2. 20
3. 77
4. 23
5. 1
6. 90

The sum is 216
```

#### Mean

Descrition: Create a list of random numbers that should have length from 0 to 10

Output the list to the console

Calculate the mean of all numbers in the list

Example:

```
./sum
1. 5
2. 20
3. 77
4. 23
5. 90

The sum is 34
```

#### Mean Sum

Descrition: Create a list of random numbers that should have length from 0 to 10

Output the list to the console

Calculate the sum of all odd numbers in the list

Calculate the mean of all even numbers in the list

Example:

```
./sum
1. 5
2. 20
3. 77
4. 23
5. 1
6. 90

The sum of odd numbers is 106
The mean of even numbers is 55
```

#### Double Down Countdown

Descrition: user enters list of length from 2 to 5 with positive numbers between 1 and 5, then the counts down starts from
number to 1 and them BOOM!

Input: (integer) countdown

Example:

```
./double_down_countdown
Countdowns [5, 2, 3]
1. Countdown: 5
Beep 5
Boop 4
Beep 3
Boop 2
Beep 1
BOOM!

2. Countdown: 2
Boop 2
Beep 1
BOOM!

3. Countdown: 3
Beep 3
Boop 2
Beep 1
BOOM!

There is nothing left!
```

#### Numeric Words

Descrition: user enters a number between -100 and 100 and program outputs same number but in words

Input: (integer) countdown

Example:

```
./numeric_words
26 is twenty six
```

```
./numeric_words
3 is three
```

```
./numeric_words
44 is fourty four
```

#### Verbal Countdown

Descrition: user enters positive number, then the count down starts from
number to 1 and them BOOM!

Input: (integer) countdown

Example:

```
./countdown
Countdown: 5
Beep five
Boop four
Beep three
Boop two
Beep one
BOOM!
```

#### Roulette 3

Description: you have a 100 usd of cash, play a roulette until you have nothing
or earner 2k in cash

Input: (integer) user's bet between 0 to 36 inclusive

Example:

```
./roulette
Your balance is 100 USD
Your bet amount is 20
Your bet number 9
Speen...
Roulette number is 9
You won 700 USD
--- Next Game ---
Your balance is 800 USD
Your bet amount is 100
Your bet number 9
Speen...
Roulette number is 10
You lose
--- Next Game ---
Your balance is 700 USD
Your bet amount is 700
Your bet number 6
Speen...
Roulette number is 9
You lose
--- Game Over ---
Your balance is 0 USD
Better luck next time
```

#### Countdown 2

Description: choose a color green 'g' or red 'r' to defuse a bomb
if succesful print hooray, otherwise start a random countdown between 5 and 9
and then detonate

Input: (char) color 'g' or 'r'

Example:

```
./countdown
Cur green ('g') or Red ('r') wire: r
Boop 6
Beep 5
Boop 4
Beep 3
Boop 2
Beep 1
BOOM!
```

```
./countdown
Cur green ('g') or Red ('r') wire: g
You did it, everyone is safe!
```
